﻿using UnityEngine;
using UnityEngine.Events;

namespace Fps
{
    [RequireComponent(typeof(Animator))]
    public class CharacterAnimationCallback : MonoBehaviour
    {
        public UnityEvent OnShootCompleteEvent { get; } = new UnityEvent();
        public UnityEvent OnReloadCompleteEvent { get; } = new UnityEvent();

        public void OnShootComplete()
        {
            OnShootCompleteEvent?.Invoke();
        }

        public void OnReloadComplete()
        {
            OnReloadCompleteEvent?.Invoke();
        }
    }
}