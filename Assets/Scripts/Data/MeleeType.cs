﻿namespace Fps
{
    public enum MeleeType
    {
        None = -1,
        Stab,
        OneHanded,
        TwoHand
    }
}