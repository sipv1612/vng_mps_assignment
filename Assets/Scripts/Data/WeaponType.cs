﻿namespace Fps
{
    public enum WeaponType
    {
        Handgun = 1,
        Auto1 = 2,
        Auto2 = 3,
        Shotgun = 4,
        Rifle1 = 5,
        Rifle2 = 6,
        SubMachineGun = 7,
        Rpg = 8,
        MachineGun = 9,
        Grenade = 10,
        Bow = 11,
        Melee = 12,
    }
}