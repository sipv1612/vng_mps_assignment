﻿using System;
using System.Diagnostics;
using System.Runtime.Serialization;
using UnityEngine;

namespace Fps
{
    [RequireComponent(typeof(Collider))]
    public class WeaponController : MonoBehaviour
    {
        [SerializeField] private Collider _pickupCollider;
        [SerializeField] private ParticleSystem _pickupEffectPrefab;
        
        public WeaponType Type;
        public MeleeType MeleeType;
        public int TotalAmmoCount;
        public int PerRoundAmmoCount;
        public int AmmoCount;
        public float Damage;

        private bool IsMelee => Type == WeaponType.Melee;
        public bool IsPlayerWeapon { get; private set; } = false;

        private bool _availableToPickup = true;
        private ParticleSystem _pickupEffect;

        private void Awake()
        {
            _pickupEffect = Instantiate(_pickupEffectPrefab.gameObject, transform).GetComponent<ParticleSystem>();
            _pickupEffect.Play();
        }

        public void Shoot()
        {
            if (AmmoCount < 0) return;
            --AmmoCount;
            --TotalAmmoCount;
        }

        public void Reload()
        {
            AmmoCount = Mathf.Clamp(TotalAmmoCount, 0, PerRoundAmmoCount);
        }

        public void Pickup(Transform parent)
        {
            if (!_availableToPickup) return;

            IsPlayerWeapon = true;
            _availableToPickup = false;
            transform.SetParent(parent, false);
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            _pickupCollider.enabled = false;
            _pickupEffect.Stop();
        }
        
        public void Drop()
        {
            IsPlayerWeapon = false;
            _availableToPickup = false;
            transform.SetParent(null);
            transform.rotation = Quaternion.identity;
            _pickupCollider.enabled = true;
            _pickupEffect.Play();
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                _availableToPickup = true;
            }
        }
    }
}