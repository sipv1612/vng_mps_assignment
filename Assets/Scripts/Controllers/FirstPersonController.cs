﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
using UnityEngine.InputSystem;
#endif

namespace Fps
{
	[RequireComponent(typeof(CharacterController))]
#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
	[RequireComponent(typeof(PlayerInput))]
#endif
	public class FirstPersonController : MonoBehaviour
	{
		#region Serialized Fields

		[Header("Player")]
		[Tooltip("Move speed of the character in m/s")]
		[SerializeField] private float MoveSpeed = 4.0f;
		[Tooltip("Sprint speed of the character in m/s")]
		[SerializeField] private float SprintSpeed = 6.0f;
		[Tooltip("Rotation speed of the character")]
		[SerializeField] private float RotationSpeed = 1.0f;
		[Tooltip("Acceleration and deceleration")]
		[SerializeField] private float SpeedChangeRate = 10.0f;
		[SerializeField] private float Health;

		[Space(10)]
		[Tooltip("The height the player can jump")]
		[SerializeField] private float JumpHeight = 1.2f;
		[Tooltip("The character uses its own gravity value. The engine default is -9.81f")]
		[SerializeField] private float Gravity = -15.0f;

		[Space(10)]
		[Tooltip("Time required to pass before being able to jump again. Set to 0f to instantly jump again")]
		[SerializeField] private float JumpTimeout = 0.1f;
		[Tooltip("Time required to pass before entering the fall state. Useful for walking down stairs")]
		[SerializeField] private float FallTimeout = 0.15f;

		[Header("Player Grounded")]
		[Tooltip("If the character is grounded or not. Not part of the CharacterController built in grounded check")]
		[SerializeField] private bool Grounded = true;
		[Tooltip("Useful for rough ground")]
		[SerializeField] private float GroundedOffset = -0.14f;
		[Tooltip("The radius of the grounded check. Should match the radius of the CharacterController")]
		[SerializeField] private float GroundedRadius = 0.5f;
		[Tooltip("What layers the character uses as ground")]
		[SerializeField] private LayerMask GroundLayers;

		[Header("Cinemachine")]
		[Tooltip("The follow target set in the Cinemachine Virtual Camera that the camera will follow")]
		[SerializeField] private GameObject CinemachineCameraTarget;
		[Tooltip("How far in degrees can you move the camera up")]
		[SerializeField] private float TopClamp = 90.0f;
		[Tooltip("How far in degrees can you move the camera down")]
		[SerializeField] private float BottomClamp = -90.0f;

		[Header("Animation")]
		[SerializeField] private Animator _animator;
		[SerializeField] private CharacterAnimationCallback _animCallback;
		
		[Header("Weapon System")]
		[SerializeField] private Transform _weaponContainer;
		[SerializeField] private WeaponController[] _weaponsHold = new WeaponController[4]; //weapon Pos 1

		#endregion

		// cinemachine
		private float _cinemachineTargetPitch;

		// player
		private float _speed;
		private float _rotationVelocity;
		private float _verticalVelocity;
		private float _terminalVelocity = 53.0f;
		private bool _isDead;
		private float _startHealth;
		private int _point;

		// timeout deltatime
		private float _jumpTimeoutDelta;
		private float _fallTimeoutDelta;

#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
		private PlayerInput _playerInput;
#endif
		private CharacterController _controller;
		private StarterAssetsInputs _input;
		private GameObject _mainCamera;

		//weapon fields
		private List<WeaponController> _pickupWeapons = new List<WeaponController>();
		private int _weaponUsingIndex = -1;
		private bool _askingForPickup;
		private bool _waitingForShootAnim;
		private bool _waitingForReload;

		private const float _threshold = 0.01f;

		private bool IsCurrentDeviceMouse
		{
			get
			{
				#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
				return _playerInput.currentControlScheme == "KeyboardMouse";
				#else
				return false;
				#endif
			}
		}

		private WeaponController Weapon => _weaponsHold[_weaponUsingIndex];

		private static readonly int KeySpeed = Animator.StringToHash("Speed_f");
		private static readonly int KeyAnimation = Animator.StringToHash("Animation_int");
		private static readonly int KeyDeath = Animator.StringToHash("Death_b");
		private static readonly int KeyJump = Animator.StringToHash("Jump_b");
		private static readonly int KeyCrouch = Animator.StringToHash("Crouch_b");
		private static readonly int KeyWeaponType = Animator.StringToHash("WeaponType_int");
		private static readonly int KeyGrounded = Animator.StringToHash("Grounded");
		private static readonly int KeyFullAuto = Animator.StringToHash("FullAuto_b");
		private static readonly int KeyShoot = Animator.StringToHash("Shoot_b");
		private static readonly int KeyReload = Animator.StringToHash("Reload_b");
		private static readonly int KeyStatic = Animator.StringToHash("Static_b");
		private static readonly int KeyHeadHorizontal = Animator.StringToHash("Head_Horizontal_f");
		private static readonly int KeyHeadVertical = Animator.StringToHash("Head_Vertical_f");
		private static readonly int KeyBlend = Animator.StringToHash("Blend(DontTouch)");
		private static readonly int KeyDeathType = Animator.StringToHash("DeathType_int");
		private static readonly int KeyBodyVertical = Animator.StringToHash("Body_Vertical_f");
		private static readonly int KeyBodyHorizontal = Animator.StringToHash("Body_Horizontal_f");
		private static readonly int KeyMeleeType = Animator.StringToHash("MeleeType_int");
		private static readonly int KeyDirection = Animator.StringToHash("Direction");

		public static FirstPersonController Instance { get; private set; }
		
		private void Awake()
		{
			if (Instance == null)
			{
				Instance = this;
			}
			else
			{
				Destroy(gameObject);
				return;
			}
			
			_startHealth = Health;

			// get a reference to our main camera
			if (_mainCamera == null)
			{
				_mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
			}

			_animCallback.OnShootCompleteEvent.AddListener(OnShootAnimationComplete);
			_animCallback.OnReloadCompleteEvent.AddListener(OnReloadAnimationComplete);
		}

		private void Start()
		{
			_controller = GetComponent<CharacterController>();
			_input = GetComponent<StarterAssetsInputs>();
#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
			_playerInput = GetComponent<PlayerInput>();
#else
			Debug.LogError( "Starter Assets package is missing dependencies. Please use Tools/Starter Assets/Reinstall Dependencies to fix it");
#endif

			// reset our timeouts on start
			_jumpTimeoutDelta = JumpTimeout;
			_fallTimeoutDelta = FallTimeout;

			for (int i = 0; i < _weaponsHold.Length; i++)
			{
				if (_weaponsHold[i] == null) continue;
				if (_weaponUsingIndex < 0)
				{
					_weaponUsingIndex = i;
					_animator.SetInteger(KeyWeaponType, (int)Weapon.Type);
					_animator.SetInteger(KeyMeleeType, (int)Weapon.MeleeType);
				}
				_weaponsHold[i].Pickup(_weaponContainer);
				_weaponsHold[i].gameObject.SetActive(_weaponUsingIndex == i);
			}
		}

		private void Update()
		{
			if (_isDead)
			{
				if (_input.pickup) SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
				return;
			}
			
			//reset jump anim
			_animator.SetBool(KeyJump, false);
			
			JumpAndGravity();
			GroundedCheck();
			Move();
			WeaponUpdate();
			UpdateAnimator();
		}

		private void LateUpdate()
		{
			CameraRotation();
		}

		private void WeaponUpdate()
		{
			if (_input.pickup && _pickupWeapons.Count > 0)
			{
				var weapon = _pickupWeapons.First();
				switch (weapon.Type)
				{
					case WeaponType.Handgun:
					case WeaponType.Bow:
						if (_weaponsHold[1] != null)
						{
							_weaponsHold[1].Drop();
						}

						_weaponsHold[1] = weapon;
						_weaponsHold[1].Pickup(_weaponContainer);
						break;
					case WeaponType.Auto1:
					case WeaponType.Auto2:
					case WeaponType.Shotgun:
					case WeaponType.Rifle1:
					case WeaponType.Rifle2:
					case WeaponType.SubMachineGun:
					case WeaponType.Rpg:
					case WeaponType.MachineGun:
						if (_weaponsHold[0] != null)
						{
							_weaponsHold[0].Drop();
						}

						_weaponsHold[0] = weapon;
						_weaponsHold[0].Pickup(_weaponContainer);
						break;
					case WeaponType.Grenade:
						if (_weaponsHold[3] != null)
						{
							_weaponsHold[3].Drop();
						}

						_weaponsHold[3] = weapon;
						_weaponsHold[3].Pickup(_weaponContainer);
						break;
					case WeaponType.Melee:
						if (_weaponsHold[2] != null)
						{
							_weaponsHold[2].Drop();
						}

						_weaponsHold[2] = weapon;
						_weaponsHold[2].Pickup(_weaponContainer);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				weapon.gameObject.SetActive(Weapon == weapon);
				_input.pickup = false;
				return;
			}

			if (_input.swapWeapon != 0)
			{
				SwapWeapon(_input.swapWeapon);
				_input.swapWeapon = 0;
			}


			if (!_waitingForShootAnim && _input.shoot)
			{
				_waitingForShootAnim = true;
				Debug.Log("Shoot");
				Weapon.Shoot();
				_animator.SetBool(KeyShoot, true);
				_input.shoot = false;
			}
		}

		private void SwapWeapon(int direction)
		{
			do
			{
				_weaponUsingIndex += (int)Mathf.Sign(direction);
				if (_weaponUsingIndex < 0)
					_weaponUsingIndex = _weaponsHold.Length - 1;
				else if (_weaponUsingIndex >= _weaponsHold.Length)
					_weaponUsingIndex = 0;
			} while (_weaponsHold[_weaponUsingIndex] == null);
			
			for (int weaponIndex = 0; weaponIndex < _weaponsHold.Length; weaponIndex++)
			{
				if (_weaponsHold[weaponIndex] == null) continue;
				_weaponsHold[weaponIndex].gameObject.SetActive(_weaponUsingIndex == weaponIndex);
			}
			
			_animator.SetInteger(KeyWeaponType, (int)Weapon.Type);
			_animator.SetInteger(KeyMeleeType, (int)Weapon.MeleeType);
			UpdatePlayerStats();
		}

		private void UpdateAnimator()
		{
			_animator.SetFloat(KeySpeed, _speed);
			_animator.SetFloat(KeyDirection, Mathf.Sign(_input.move.y));
		}

		private void GroundedCheck()
		{
			// set sphere position, with offset
			Vector3 spherePosition = new Vector3(transform.position.x, transform.position.y - GroundedOffset, transform.position.z);
			Grounded = Physics.CheckSphere(spherePosition, GroundedRadius, GroundLayers, QueryTriggerInteraction.Ignore);
		}

		private void CameraRotation()
		{
			// if there is an input
			if (_input.look.sqrMagnitude >= _threshold)
			{
				//Don't multiply mouse input by Time.deltaTime
				float deltaTimeMultiplier = IsCurrentDeviceMouse ? 1.0f : Time.deltaTime;
				
				_cinemachineTargetPitch += _input.look.y * RotationSpeed * deltaTimeMultiplier;
				_rotationVelocity = _input.look.x * RotationSpeed * deltaTimeMultiplier;

				// clamp our pitch rotation
				_cinemachineTargetPitch = ClampAngle(_cinemachineTargetPitch, BottomClamp, TopClamp);

				// Update Cinemachine camera target pitch
				CinemachineCameraTarget.transform.localRotation = Quaternion.Euler(_cinemachineTargetPitch, 0.0f, 0.0f);

				// rotate the player left and right
				transform.Rotate(Vector3.up * _rotationVelocity);
			}
		}

		private void Move()
		{
			// set target speed based on move speed, sprint speed and if sprint is pressed
			float targetSpeed = _input.sprint ? SprintSpeed : MoveSpeed;

			// a simplistic acceleration and deceleration designed to be easy to remove, replace, or iterate upon

			// note: Vector2's == operator uses approximation so is not floating point error prone, and is cheaper than magnitude
			// if there is no input, set the target speed to 0
			if (_input.move == Vector2.zero) targetSpeed = 0.0f;

			// a reference to the players current horizontal velocity
			float currentHorizontalSpeed = new Vector3(_controller.velocity.x, 0.0f, _controller.velocity.z).magnitude;

			float speedOffset = 0.1f;
			float inputMagnitude = _input.analogMovement ? _input.move.magnitude : 1f;

			// accelerate or decelerate to target speed
			if (currentHorizontalSpeed < targetSpeed - speedOffset || currentHorizontalSpeed > targetSpeed + speedOffset)
			{
				// creates curved result rather than a linear one giving a more organic speed change
				// note T in Lerp is clamped, so we don't need to clamp our speed
				_speed = Mathf.Lerp(currentHorizontalSpeed, targetSpeed * inputMagnitude, Time.deltaTime * SpeedChangeRate);

				// round speed to 3 decimal places
				_speed = Mathf.Round(_speed * 1000f) / 1000f;
			}
			else
			{
				_speed = targetSpeed;
			}

			// normalise input direction
			Vector3 inputDirection = new Vector3(_input.move.x, 0.0f, _input.move.y).normalized;

			// note: Vector2's != operator uses approximation so is not floating point error prone, and is cheaper than magnitude
			// if there is a move input rotate player when the player is moving
			if (_input.move != Vector2.zero)
			{
				// move
				inputDirection = transform.right * _input.move.x + transform.forward * _input.move.y;
			}

			// move the player
			_controller.Move(inputDirection.normalized * (_speed * Time.deltaTime) + new Vector3(0.0f, _verticalVelocity, 0.0f) * Time.deltaTime);
		}

		private void JumpAndGravity()
		{
			if (Grounded)
			{
				// reset the fall timeout timer
				_fallTimeoutDelta = FallTimeout;

				// stop our velocity dropping infinitely when grounded
				if (_verticalVelocity < 0.0f)
				{
					_verticalVelocity = -2f;
				}

				// Jump
				if (_input.jump && _jumpTimeoutDelta <= 0.0f)
				{
					// the square root of H * -2 * G = how much velocity needed to reach desired height
					_animator.SetBool(KeyJump, true);
					_verticalVelocity = Mathf.Sqrt(JumpHeight * -2f * Gravity);
				}

				// jump timeout
				if (_jumpTimeoutDelta >= 0.0f)
				{
					_jumpTimeoutDelta -= Time.deltaTime;
				}
			}
			else
			{
				// reset the jump timeout timer
				_jumpTimeoutDelta = JumpTimeout;

				// fall timeout
				if (_fallTimeoutDelta >= 0.0f)
				{
					_fallTimeoutDelta -= Time.deltaTime;
				}

				// if we are not grounded, do not jump
				_input.jump = false;
			}

			// apply gravity over time if under terminal (multiply by delta time twice to linearly speed up over time)
			if (_verticalVelocity < _terminalVelocity)
			{
				_verticalVelocity += Gravity * Time.deltaTime;
			}
		}

		private static float ClampAngle(float lfAngle, float lfMin, float lfMax)
		{
			if (lfAngle < -360f) lfAngle += 360f;
			if (lfAngle > 360f) lfAngle -= 360f;
			return Mathf.Clamp(lfAngle, lfMin, lfMax);
		}

		private void OnDrawGizmosSelected()
		{
			Color transparentGreen = new Color(0.0f, 1.0f, 0.0f, 0.35f);
			Color transparentRed = new Color(1.0f, 0.0f, 0.0f, 0.35f);

			if (Grounded) Gizmos.color = transparentGreen;
			else Gizmos.color = transparentRed;

			// when selected, draw a gizmo in the position of, and matching radius of, the grounded collider
			Gizmos.DrawSphere(new Vector3(transform.position.x, transform.position.y - GroundedOffset, transform.position.z), GroundedRadius);
		}

		private void OnTriggerEnter(Collider other)
		{
			if (other.CompareTag("Weapon") || other.CompareTag("DamageWeapon"))
			{
				var weapon = other.GetComponent<WeaponController>();
				if (weapon != null && !_pickupWeapons.Contains(weapon))
				{
					_pickupWeapons.Add(weapon);
					_askingForPickup = true;
					UICanvasControllerInput.Instance.ShowPickupPopup();
				}
			}
		}

		private void OnTriggerExit(Collider other)
		{
			if (other.CompareTag("Weapon") || other.CompareTag("DamageWeapon"))
			{
				var weapon = other.GetComponent<WeaponController>();
				if (weapon != null && _pickupWeapons.Contains(weapon))
				{
					_pickupWeapons.Remove(weapon);
					if (_pickupWeapons.Count == 0)
					{
						_askingForPickup = false;
						UICanvasControllerInput.Instance.HidePickupPopup();
					}
				}
			}
		}

		private void OnShootAnimationComplete()
		{
			_waitingForShootAnim = false;
			_animator.SetBool(KeyShoot, false);
			if (Weapon.AmmoCount == 0)
			{
				if (Weapon.TotalAmmoCount == 0)
				{
					Destroy(Weapon.gameObject);
					_weaponsHold[_weaponUsingIndex] = null;
					SwapWeapon(1);
					return;
				}

				_waitingForReload = true;
				_animator.SetBool(KeyReload, true);
			}
			UpdatePlayerStats();
		}

		private void OnReloadAnimationComplete()
		{
			_waitingForReload = false;
			_animator.SetBool(KeyReload, false);
			Weapon.Reload();
		}

		public void TakeDamage(float damage)
		{
			if (Health <= 0) return;
			
			Health -= damage;
			if (Health <= 0)
			{
				_isDead = true;
				_animator.SetBool(KeyDeath, true);
				UICanvasControllerInput.Instance.ShowGameOverPopup();
			}
			UpdatePlayerStats();
		}

		private void UpdatePlayerStats()
		{
			UICanvasControllerInput.Instance.UpdatePlayerStats(Mathf.Clamp01(Health / _startHealth), Weapon.AmmoCount, Weapon.PerRoundAmmoCount, Weapon.TotalAmmoCount);
		}
	}
}