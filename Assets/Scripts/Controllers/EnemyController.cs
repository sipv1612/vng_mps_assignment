﻿using System;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Fps
{
    [RequireComponent(typeof(Collider), typeof(NavMeshAgent))]
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private float _walkPointRange = 5f;
        [SerializeField] private float _timeBetweenAttacks = 1f;
        [SerializeField] private float _attackRange = 2f;
        [SerializeField] private float _sightRange = 5f;
        [SerializeField] private float _startHealth;
        [SerializeField] private float _damage;
        [SerializeField] private int _onKillPoint;
        [SerializeField] public LayerMask _groundMask, _playerMask;
        [SerializeField] private GameObject _healthBarPrefab;
        
        public bool _playerInSightRange, _playerInAttackRange;
        private NavMeshAgent _agent;
        private Vector3 _walkPoint;
        private bool _walkPointSet;
        private bool _alreadyAttacked;
        private float _health;
        private Slider _healthBar;

        private void Awake()
        {
            _healthBar = Instantiate(_healthBarPrefab, transform).GetComponentInChildren<Slider>();
            _agent = GetComponent<NavMeshAgent>();
            _health = _startHealth;
            _healthBar.value = Mathf.Clamp01(_health / _startHealth);
        }

        private void Update()
        {
            _playerInSightRange = Physics.CheckSphere(transform.position, _sightRange, _playerMask);
            _playerInAttackRange = Physics.CheckSphere(transform.position, _attackRange, _playerMask);
            
            if (!_playerInSightRange && !_playerInAttackRange) Patrol();
            if (_playerInSightRange && !_playerInAttackRange) Chase();
            if (_playerInAttackRange && _playerInSightRange) Attack();
        }

        private void Patrol()
        {
            if (!_walkPointSet) SearchWalkPoint();

            if (_walkPointSet)
                _agent.SetDestination(_walkPoint);

            var distanceToWalkPoint = transform.position - _walkPoint;

            //Walk point reached
            if (distanceToWalkPoint.magnitude < 1f)
                _walkPointSet = false;
        }

        private void SearchWalkPoint()
        {
            //Calculate random point in range
            var randomZ = Random.Range(-_walkPointRange, _walkPointRange);
            var randomX = Random.Range(-_walkPointRange, _walkPointRange);

            _walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

            if (Physics.Raycast(_walkPoint, -transform.up, 2f, _groundMask))
                _walkPointSet = true;
        }

        private void Chase()
        {
            if (FirstPersonController.Instance == null) return;

            _agent.SetDestination(FirstPersonController.Instance.transform.position);
        }

        private void Attack()
        {
            if (FirstPersonController.Instance == null || _alreadyAttacked) return;

            _agent.SetDestination(FirstPersonController.Instance.transform.position);
            transform.LookAt(FirstPersonController.Instance.transform);

            FirstPersonController.Instance.TakeDamage(_damage);
            
            _alreadyAttacked = true;
            Invoke(nameof(ResetAttack), _timeBetweenAttacks);
        }

        public void ResetAttack()
        {
            _alreadyAttacked = false;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("DamageWeapon"))
            {
                var damageWeapon = other.GetComponent<WeaponController>();
                if (!damageWeapon.IsPlayerWeapon) return;
                
                Debug.Log($"{name} Trigger {other.name} type {other.GetType().Name}");
                TakeDamage(damageWeapon.Damage);
            }
        }

        private void TakeDamage(float damage)
        {
            _health -= damage;
            _healthBar.value = Mathf.Clamp01(_health / _startHealth);

            if (_health <= 0) Invoke(nameof(DestroyEnemy), 0.5f);
        }
        
        private void DestroyEnemy()
        {
            UICanvasControllerInput.Instance.AddPoint(_onKillPoint);
            Destroy(gameObject);
        }

    }
}