using System;
using UnityEngine;
using UnityEngine.UI;

namespace Fps
{
    public class UICanvasControllerInput : MonoBehaviour
    {
        [Header("Output")]
        public StarterAssetsInputs starterAssetsInputs;

        [Header("Control HUD")]
        [SerializeField] private GameObject _pickupPopup;
        [SerializeField] private GameObject _gameOverPopup;
        [SerializeField] private Slider _healthBar;
        [SerializeField] private Text _pointText;
        [SerializeField] private Text _ammoText;
        [SerializeField] private Text _totalAmmoText;

        private int _point = 0;

        public static UICanvasControllerInput Instance { get; private set; }
        
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
                return;
            }
            
            _pointText.text = $"Point: {_point}";
            _pickupPopup.SetActive(false);
            _gameOverPopup.SetActive(false);
        }

        public void VirtualMoveInput(Vector2 virtualMoveDirection)
        {
            starterAssetsInputs.MoveInput(virtualMoveDirection);
        }

        public void VirtualLookInput(Vector2 virtualLookDirection)
        {
            starterAssetsInputs.LookInput(virtualLookDirection);
        }

        public void VirtualJumpInput(bool virtualJumpState)
        {
            starterAssetsInputs.JumpInput(virtualJumpState);
        }

        public void VirtualSprintInput(bool virtualSprintState)
        {
            starterAssetsInputs.SprintInput(virtualSprintState);
        }

        public void VirtualShootInput(bool virtualShootState)
        {
            starterAssetsInputs.ShootInput(virtualShootState);
        }

        public void VirtualPickupInput(bool virtualPickupState)
        {
            starterAssetsInputs.PickupInput(virtualPickupState);
        }

        public void VirtualSwapInput(bool virtualSwapState)
        {
            starterAssetsInputs.SwapRightInput(virtualSwapState);
        }

        public void ShowPickupPopup()
        {
            _pickupPopup.SetActive(true);
        }
        
        public void ShowGameOverPopup()
        {
            _gameOverPopup.SetActive(true);
        }

        public void HidePickupPopup()
        {
            _pickupPopup.SetActive(false);
        }

        public void UpdatePlayerStats(float playerHealthPercent, int ammo, int ammoRoundCount, int totalAmmo)
        {
            _healthBar.value = playerHealthPercent;
            if (ammo > 0)
            {
                _ammoText.enabled = true;
                _totalAmmoText.enabled = true;
                _ammoText.text = $"Ammo: {ammo}/{ammoRoundCount}";
                _totalAmmoText.text = $"Total Ammo: {totalAmmo}";
            }
            else
            {
                _ammoText.enabled = false;
                _totalAmmoText.enabled = false;
            }
        }

        public void AddPoint(int point)
        {
            _point += point;
            _pointText.text = $"Point: {_point}";
        }
    }
}
